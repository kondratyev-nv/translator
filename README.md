# Translator #

Тестовая реализация переводчика с интерфейсом коммандной строки, Web API и подключаемыми модулями

### Основные модули ###

* `src/Translator.PluginContracts` - Контракты для подключаемых модулей
* `src/Translator.WebApi` - Web API сервиса перевода
* `src/Translator.ConsoleApplication` - Консольное приложение
* `src/Translator.Core` - Базовая реализация сервисов перевода

### Подключаемые модули ###

* `plugins/EnTranslatorPlugin` - Пример подключаемого модуля

### Консольное приложение ###

При запуске приложения происходит загрузка подключаемых модулей. Пользователь взаимодействует с программой посредством комманд.

Список доступных комманд:

* `exit` - Завершить работу приложения
* `select <язык>` - Выбрать язык для перевода
* `list` - Отобразить список доступных языков

Остальной ввод пользователя трактуется как русский текст для перевода на выбранный язык.

### Web API ###

Для получения перевода через Web API необходимо отправить

* GET запрос на `/api/translator/<язык>?q=<текст для перевода>`
* POST запрос на `/api/translator/<язык>` с указанием `Content-Type: application/json` и телом сообщения `{ "request": <текст для перевода> }`.

Для получения списка доступных языков и языка системы по умолчанию нужно отправить GET запрос на `/api/translator`.
