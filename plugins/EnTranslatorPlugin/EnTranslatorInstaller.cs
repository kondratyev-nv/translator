﻿using Translator.PluginContracts;

namespace EnTranslatorPlugin
{
    public class EnTranslatorInstaller : IPluginInstaller
    {
        /// <summary>Загрузить плагины в хост</summary>
        /// <param name="host">Хост</param>
        public void Install(ITranslatorPluginHost host)
        {
            host.Register("en", new EnTranslator());
        }
    }
}