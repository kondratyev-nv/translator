﻿using System;
using System.Collections.Generic;
using System.Linq;
using Translator.PluginContracts;

namespace EnTranslatorPlugin
{
    public class EnTranslator : ITranslator
    {
        private readonly Dictionary<string, string> _words = new Dictionary<string, string>
        {
            {"привет", "hello"},
            {"мир", "world"}
        };

        /// <summary>Перевести выражение</summary>
        /// <param name="input">Выражение</param>
        /// <returns>Перевод на языке плагина</returns>
        public string Translate(string input)
        {
            var punctuations = input.Where(char.IsPunctuation).Distinct().ToArray();
            var translation =
                input.Split(punctuations, StringSplitOptions.RemoveEmptyEntries)
                     .Select(w => w.Trim().ToLower())
                     .Select(w => _words.ContainsKey(w) ? _words[w] : w);
            return string.Join(" ", translation);
        }
    }
}