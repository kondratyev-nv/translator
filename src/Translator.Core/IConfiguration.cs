namespace Translator.Core
{
    /// <summary>Конфигурация приложения</summary>
    public interface IConfiguration
    {
        /// <summary>Получить значение по ключу</summary>
        string Get(string key);

        /// <summary>Получить типизированное значение по ключу</summary>
        T Get<T>(string key);
    }
}