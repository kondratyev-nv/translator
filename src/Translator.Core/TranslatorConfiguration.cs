﻿namespace Translator.Core
{
    /// <summary>Конфигурация переводчика</summary>
    public class TranslatorConfiguration
    {
        /// <summary>Путь к плагинам</summary>
        public string PluginsPath { get; set; }

        /// <summary>Язык по умолчанию</summary>
        public string DefaultLang { get; set; }

        /// <summary>Возможно ли использовать любой доступный язык, если язык по умолчанию не задан</summary>
        public bool AnyDefaultLang { get; set; }
    }
}