﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    /// <summary>Песочница для загрузки плагинов</summary>
    [SecurityCritical]
    internal class SandboxPluginLoader : MarshalByRefObject
    {
        private const string DomainName = "Sandbox";

        /// <summary>Загрузить плагины из сборки</summary>
        /// <param name="assemblyPath">Пусть к сборке</param>
        public static IReadOnlyDictionary<string, ITranslator> LoadPlugins(string assemblyPath)
        {
            var setup = new AppDomainSetup
            {
                ApplicationBase = Path.GetDirectoryName(assemblyPath),
                ApplicationName = DomainName,
                DisallowBindingRedirects = true,
                DisallowCodeDownload = true,
                DisallowPublisherPolicy = true
            };

            var permissions = new PermissionSet(PermissionState.None);
            permissions.AddPermission(new SecurityPermission(SecurityPermissionFlag.Execution));
            permissions.AddPermission(
                new FileIOPermission(FileIOPermissionAccess.Read | FileIOPermissionAccess.PathDiscovery, assemblyPath));

            var domain = AppDomain.CreateDomain(DomainName, null, setup, permissions,
                                                typeof(SandboxPluginLoader).Assembly.Evidence
                                                                           .GetHostEvidence<StrongName>());
            var sandbox =
                (SandboxPluginLoader)
                Activator.CreateInstanceFrom(domain,
                                             typeof(SandboxPluginLoader).Assembly.ManifestModule.FullyQualifiedName,
                                             typeof(SandboxPluginLoader).FullName).Unwrap();

            return sandbox.GetPlugins(assemblyPath);
        }

        private IReadOnlyDictionary<string, ITranslator> GetPlugins(string assemblyPath)
        {
            var sandboxPluginHost = new SandboxPluginHost();
            var installers =
                Assembly.LoadFile(assemblyPath)
                        .GetTypes()
                        .Where(t => typeof(IPluginInstaller).IsAssignableFrom(t))
                        .Where(t => !t.IsAbstract)
                        .Where(t => t.GetConstructor(Type.EmptyTypes) != null)
                        .Select(t => (IPluginInstaller) Activator.CreateInstance(t));
            foreach (var installer in installers)
            {
                installer.Install(sandboxPluginHost);
            }

            return sandboxPluginHost.GetTranslators();
        }

        [SecurityCritical]
        private class SandboxPluginHost : MarshalByRefObject, ITranslatorPluginHost
        {
            private readonly Dictionary<string, ITranslator> _translators = new Dictionary<string, ITranslator>();

            /// <summary>Получить все зарегистрированные плагины</summary>
            /// <returns>Код языка и соответствующий ему плагин</returns>
            public IReadOnlyDictionary<string, ITranslator> GetTranslators()
            {
                return _translators;
            }

            /// <summary>Зарегистрировать плагин перевода</summary>
            /// <param name="code">Код языка</param>
            /// <param name="module">Плагин перевода</param>
            public void Register(string code, ITranslator module)
            {
                _translators.Add(code, new SandboxTranslator(module));
            }
        }

        [SecurityCritical]
        private class SandboxTranslator : MarshalByRefObject, ITranslator
        {
            private readonly ITranslator _translator;

            public SandboxTranslator(ITranslator translator)
            {
                _translator = translator;
            }

            /// <summary>Перевести выражение</summary>
            /// <param name="input">Выражение</param>
            /// <returns>Перевод на языке плагина</returns>
            public string Translate(string input)
            {
                return _translator.Translate(input);
            }
        }
    }
}