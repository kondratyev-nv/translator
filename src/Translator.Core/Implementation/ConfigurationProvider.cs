namespace Translator.Core.Implementation
{
    public class ConfigurationProvider : IConfigurationProvider
    {
        private const string DefaultPluginsPath = "Plugins";

        private readonly IConfiguration _configuration;

        public ConfigurationProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public TranslatorConfiguration GetConfiguration()
        {
            var anyDefaultLang = _configuration.Get("AnyDefaultLang");
            return new TranslatorConfiguration
            {
                PluginsPath = _configuration.Get("PluginsPath") ?? DefaultPluginsPath,
                DefaultLang = _configuration.Get("DefaultLang"),
                AnyDefaultLang = anyDefaultLang != null && bool.Parse(anyDefaultLang)
            };
        }
    }
}