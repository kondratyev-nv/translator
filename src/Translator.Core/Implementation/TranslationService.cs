using System;
using System.Collections.Generic;
using System.Linq;
using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    public class TranslationService : ITranslationService
    {
        private readonly string _defaultLang;
        private readonly Dictionary<string, ITranslator> _translators;

        public TranslationService(ITranslatorPluginHost host, IDefaultLanguageProvider defaultLanguageProvider)
        {
            if (host == null)
            {
                throw new ArgumentNullException(nameof(host));
            }
            if (defaultLanguageProvider == null)
            {
                throw new ArgumentNullException(nameof(defaultLanguageProvider));
            }

            _translators = host.GetTranslators().ToDictionary(x => x.Key, x => x.Value);
            _defaultLang = defaultLanguageProvider.GetDefaultLang();
            if (string.IsNullOrWhiteSpace(_defaultLang))
            {
                throw new ArgumentException("No default language provided");
            }
            if (!_translators.ContainsKey(_defaultLang))
            {
                throw new ArgumentException($"Could not use \"{_defaultLang}\" as default language. " +
                                            "No translator for code specified.");
            }
        }

        public TranslationResult Translate(string lang, string input)
        {
            var resultLang = _translators.ContainsKey(lang) ? lang : _defaultLang;
            var translator = _translators[resultLang];
            return new TranslationResult {Lang = resultLang, Result = translator.Translate(input)};
        }

        public string[] AvailableLangs()
        {
            return _translators.Keys.ToArray();
        }

        public string GetDefaultLang()
        {
            return _defaultLang;
        }
    }
}