﻿using System;
using System.IO;
using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    /// <summary>Сервис загрузки плагинов</summary>
    public class TranslatorPluginLoader : ITranslatorPluginLoader
    {
        private readonly ILogger<TranslatorPluginLoader> _logger;

        public TranslatorPluginLoader(ILogger<TranslatorPluginLoader> logger)
        {
            _logger = logger;
        }

        /// <summary>Загрузить все плагины по указанному пути в хост</summary>
        /// <param name="path">Путь</param>
        /// <param name="host">Хост</param>
        public void Load(string path, ITranslatorPluginHost host)
        {
            foreach (var file in Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories))
            {
                var assemblyPath = Path.GetFullPath(file);
                try
                {
                    var translators = SandboxPluginLoader.LoadPlugins(assemblyPath);
                    foreach (var translator in translators)
                    {
                        host.Register(translator.Key, translator.Value);
                    }
                }
                catch (Exception ex)
                {
                    _logger.Warning($"Could not load plugin {assemblyPath}. Reason: {ex.Message}");
                }
            }
        }
    }
}