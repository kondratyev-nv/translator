﻿using System.Collections.Generic;
using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    /// <summary>Хост для плагинов</summary>
    public class TranslatorPluginHost : ITranslatorPluginHost
    {
        private readonly Dictionary<string, ITranslator> _translators = new Dictionary<string, ITranslator>();

        /// <summary>Получить все зарегистрированные плагины</summary>
        /// <returns>Код языка и соответствующий ему плагин</returns>
        public IReadOnlyDictionary<string, ITranslator> GetTranslators()
        {
            return _translators;
        }

        /// <summary>Зарегистрировать плагин перевода</summary>
        /// <param name="code">Код языка</param>
        /// <param name="module">Плагин перевода</param>
        public void Register(string code, ITranslator module)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                throw new TranslatorRegistrationException("Can not register translator with empty code");
            }
            if (_translators.ContainsKey(code))
            {
                throw new TranslatorRegistrationException($"Translator with code \"{code}\" already registered");
            }
            if (module == null)
            {
                throw new TranslatorRegistrationException("Can not register empty translator module");
            }

            _translators[code] = module;
        }
    }
}