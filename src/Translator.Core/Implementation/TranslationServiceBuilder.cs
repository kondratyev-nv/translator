using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    public class TranslationServiceBuilder : ITranslationServiceBuilder
    {
        private readonly ITranslatorPluginHost _host;

        private readonly ITranslatorPluginLoader _loader;

        private IDefaultLanguageProvider _defaultLanguageProvider;

        public TranslationServiceBuilder(ITranslatorPluginHost host, ITranslatorPluginLoader loader)
        {
            _host = host;
            _loader = loader;
        }

        public ITranslationServiceBuilder WithPluginsFrom(string path)
        {
            _loader.Load(path, _host);
            return this;
        }

        public ITranslationServiceBuilder WithDefaultLanguageProvider(IDefaultLanguageProvider defaultLanguageProvider)
        {
            _defaultLanguageProvider = defaultLanguageProvider;
            return this;
        }

        public ITranslationService Build()
        {
            return new TranslationService(_host, _defaultLanguageProvider);
        }
    }
}