using System;
using System.Linq;
using Translator.PluginContracts;

namespace Translator.Core.Implementation
{
    public class DefaultLanguageProvider : IDefaultLanguageProvider
    {
        private const string CouldNotDetectDefaultLanguage = "Could not detect default language";

        private readonly ITranslatorPluginHost _host;
        private readonly bool _anyDefaultLang;
        private string _defaultLang;

        public DefaultLanguageProvider(ITranslatorPluginHost host, IConfigurationProvider configurationProvider)
        {
            if (host == null)
            {
                throw new ArgumentNullException(nameof(host));
            }
            if (configurationProvider == null)
            {
                throw new ArgumentNullException(nameof(configurationProvider));
            }

            var configuration = configurationProvider.GetConfiguration();
            _host = host;
            _anyDefaultLang = configuration.AnyDefaultLang;
            _defaultLang = configuration.DefaultLang;
            if (string.IsNullOrWhiteSpace(_defaultLang) && !_anyDefaultLang)
            {
                throw new InvalidOperationException(CouldNotDetectDefaultLanguage);
            }
        }

        public string GetDefaultLang()
        {
            TryFillDefaultLang();
            ValidateDefaultLang();

            return _defaultLang;
        }

        private void ValidateDefaultLang()
        {
            if (string.IsNullOrWhiteSpace(_defaultLang))
            {
                throw new InvalidOperationException(CouldNotDetectDefaultLanguage);
            }

            var translators = _host.GetTranslators();
            if (!translators.ContainsKey(_defaultLang))
            {
                throw new InvalidOperationException(CouldNotDetectDefaultLanguage);
            }
        }

        private void TryFillDefaultLang()
        {
            if (!string.IsNullOrWhiteSpace(_defaultLang))
            {
                return;
            }

            if (!_anyDefaultLang)
            {
                throw new InvalidOperationException(CouldNotDetectDefaultLanguage);
            }

            var translators = _host.GetTranslators();
            _defaultLang = translators.Keys.FirstOrDefault();
            if (_defaultLang == null)
            {
                throw new InvalidOperationException(CouldNotDetectDefaultLanguage);
            }
        }
    }
}