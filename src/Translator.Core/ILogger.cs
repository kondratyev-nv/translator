namespace Translator.Core
{
    /// <summary>Логгер</summary>
    public interface ILogger<T>
    {
        /// <summary>Предупреждение</summary>
        void Warning(string message);

        /// <summary>Ошибка</summary>
        void Error(string message);

        /// <summary>Информация</summary>
        void Info(string message);
    }
}