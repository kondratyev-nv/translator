namespace Translator.Core
{
    /// <summary>Результат перевода</summary>
    public class TranslationResult
    {
        /// <summary>Язык на котором сформирован перевод</summary>
        public string Lang { get; set; }

        /// <summary>Результат</summary>
        public string Result { get; set; }
    }
}