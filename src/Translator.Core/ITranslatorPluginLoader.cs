using Translator.PluginContracts;

namespace Translator.Core
{
    /// <summary>Сервис загрузки плагинов</summary>
    public interface ITranslatorPluginLoader
    {
        /// <summary>Загрузить все плагины по указанному пути в хост</summary>
        /// <param name="path">Путь</param>
        /// <param name="host">Хост</param>
        void Load(string path, ITranslatorPluginHost host);
    }
}