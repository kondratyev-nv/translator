namespace Translator.Core
{
    /// <summary>Сервис перевода</summary>
    public interface ITranslationService
    {
        /// <summary>Перевести фразу на указанный язык</summary>
        /// <param name="lang">Язык</param>
        /// <param name="input">Текст</param>
        /// <returns>Результат перевода</returns>
        TranslationResult Translate(string lang, string input);

        /// <summary>Получить доступные языки</summary>
        string[] AvailableLangs();

        /// <summary>Получить язык по умолчанию</summary>
        string GetDefaultLang();
    }
}