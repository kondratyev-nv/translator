namespace Translator.Core
{
    /// <summary>Сервис формирования переводчика</summary>
    public interface ITranslationServiceBuilder
    {
        /// <summary>Загрузить плагины по указанному пути</summary>
        ITranslationServiceBuilder WithPluginsFrom(string path);

        /// <summary>Установить сервис получения языка по умолчанию</summary>
        ITranslationServiceBuilder WithDefaultLanguageProvider(IDefaultLanguageProvider defaultLanguageProvider);

        /// <summary>Сформировать переводчик</summary>
        ITranslationService Build();
    }
}