namespace Translator.Core
{
    /// <summary>Сервис предоставления конфигурации</summary>
    public interface IConfigurationProvider
    {
        /// <summary>Получить конфигурацию</summary>
        TranslatorConfiguration GetConfiguration();
    }
}