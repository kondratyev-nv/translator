namespace Translator.Core
{
    /// <summary>Сервис получения языка по умолчанию</summary>
    public interface IDefaultLanguageProvider
    {
        /// <summary>Получить язык по умолчанию</summary>
        string GetDefaultLang();
    }
}