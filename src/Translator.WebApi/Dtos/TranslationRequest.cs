﻿namespace Translator.WebApi.Dtos
{
    /// <summary>Запрос на перевод текста</summary>
    public class TranslationRequest
    {
        /// <summary>Текст</summary>
        public string Request { get; set; }
    }
}