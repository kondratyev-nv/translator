﻿namespace Translator.WebApi.Dtos
{
    /// <summary>Информация о доступных переводчиках</summary>
    public class ListTranslatorsResponse
    {
        /// <summary>Язык по умолчанию</summary>
        public string DefaultLang { get; set; }

        /// <summary>Список доступных языков</summary>
        public string[] AvailableLangs { get; set; }
    }
}