﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Translator.Core;
using Translator.Core.Implementation;
using Translator.PluginContracts;
using Translator.WebApi.Services;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;

namespace Translator.WebApi
{
    public class Startup
    {
        private readonly IConfigurationRoot _configuration;

        public Startup(IHostingEnvironment env)
        {
            var builder =
                new ConfigurationBuilder().SetBasePath(env.ContentRootPath)
                                          .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                                          .AddEnvironmentVariables();
            _configuration = builder.Build();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(_configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddSingleton<ITranslationServiceBuilder, TranslationServiceBuilder>();
            services.AddSingleton<ITranslatorPluginLoader, TranslatorPluginLoader>();
            services.AddSingleton<ITranslatorPluginHost, TranslatorPluginHost>();
            services.AddSingleton(typeof(Core.ILogger<>), typeof(DefaultMsLogger<>));
            services.AddSingleton<ITranslationService, WebTranslationService>();
            services.AddSingleton<IDefaultLanguageProvider, DefaultLanguageProvider>();
            services.AddSingleton<Core.IConfigurationProvider, Core.Implementation.ConfigurationProvider>();
            services.Add(new ServiceDescriptor(typeof(IConfiguration), provider => _configuration,
                                               ServiceLifetime.Singleton));
            services.AddSingleton<Core.IConfiguration, WebApiConfiguration>();
        }
    }
}