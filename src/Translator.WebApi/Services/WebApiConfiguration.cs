using Microsoft.Extensions.Configuration;

namespace Translator.WebApi.Services
{
    public class WebApiConfiguration : Core.IConfiguration
    {
        private readonly IConfiguration _configuration;

        public WebApiConfiguration(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>Получить значение по ключу</summary>
        public string Get(string key)
        {
            return _configuration[key];
        }

        /// <summary>Получить типизированное значение по ключу</summary>
        public T Get<T>(string key)
        {
            return _configuration.GetValue<T>(key);
        }
    }
}