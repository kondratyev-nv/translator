using Microsoft.Extensions.Logging;

namespace Translator.WebApi.Services
{
    public class DefaultMsLogger<T> : Core.ILogger<T>
    {
        private readonly ILogger<T> _logger;

        public DefaultMsLogger(ILogger<T> logger)
        {
            _logger = logger;
        }

        /// <summary>Ошибка</summary>
        public void Error(string message)
        {
            _logger.LogError(message);
        }

        /// <summary>Информация</summary>
        public void Info(string message)
        {
            _logger.LogInformation(message);
        }

        /// <summary>Предупреждение</summary>
        public void Warning(string message)
        {
            _logger.LogWarning(message);
        }
    }
}