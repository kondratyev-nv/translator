using System;
using Translator.Core;

namespace Translator.WebApi.Services
{
    /// <summary>Сервис перевода</summary>
    public class WebTranslationService : ITranslationService
    {
        private readonly ITranslationService _service;

        public WebTranslationService(ITranslationServiceBuilder builder, IConfigurationProvider configurationProvider,
                                     IDefaultLanguageProvider defaultLanguageProvider)
        {
            if (builder == null)
            {
                throw new ArgumentNullException(nameof(builder));
            }
            if (configurationProvider == null)
            {
                throw new ArgumentNullException(nameof(configurationProvider));
            }
            if (defaultLanguageProvider == null)
            {
                throw new ArgumentNullException(nameof(defaultLanguageProvider));
            }

            var configuration = configurationProvider.GetConfiguration();
            _service =
                builder.WithDefaultLanguageProvider(defaultLanguageProvider)
                       .WithPluginsFrom(configuration.PluginsPath)
                       .Build();
        }

        /// <summary>Получить доступные языки</summary>
        public string[] AvailableLangs()
        {
            return _service.AvailableLangs();
        }

        /// <summary>Получить язык по умолчанию</summary>
        public string GetDefaultLang()
        {
            return _service.GetDefaultLang();
        }

        /// <summary>Перевести фразу на указанный язык</summary>
        /// <param name="lang">Язык</param>
        /// <param name="input">Текст</param>
        /// <returns>Результат перевода</returns>
        public TranslationResult Translate(string lang, string input)
        {
            return _service.Translate(lang, input);
        }
    }
}