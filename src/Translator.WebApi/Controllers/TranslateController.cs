﻿using Microsoft.AspNetCore.Mvc;
using Translator.Core;
using Translator.WebApi.Dtos;

namespace Translator.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class TranslatorController : Controller
    {
        private readonly ITranslationService _service;

        public TranslatorController(ITranslationService service)
        {
            _service = service;
        }

        // GET api/translator
        [HttpGet]
        public ListTranslatorsResponse List()
        {
            return new ListTranslatorsResponse
            {
                DefaultLang = _service.GetDefaultLang(),
                AvailableLangs = _service.AvailableLangs()
            };
        }

        // GET api/translator/<lang>?q=<query>
        [HttpGet("{lang}")]
        public TranslationResult Get(string lang, string q)
        {
            return _service.Translate(lang, q);
        }

        // POST api/translator/<lang>
        [HttpPost("{lang}")]
        public TranslationResult Post(string lang, [FromBody] TranslationRequest request)
        {
            return _service.Translate(lang, request.Request);
        }
    }
}