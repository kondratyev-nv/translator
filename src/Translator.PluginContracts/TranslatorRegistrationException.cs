using System;

namespace Translator.PluginContracts
{
    public class TranslatorRegistrationException : Exception
    {
        public TranslatorRegistrationException()
        {
        }

        public TranslatorRegistrationException(string message) : base(message)
        {
        }
    }
}