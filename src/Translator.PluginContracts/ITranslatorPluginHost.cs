using System.Collections.Generic;

namespace Translator.PluginContracts
{
    /// <summary>Хост для плагинов</summary>
    public interface ITranslatorPluginHost
    {
        /// <summary>Зарегистрировать плагин перевода</summary>
        /// <param name="code">Код языка</param>
        /// <param name="module">Плагин перевода</param>
        void Register(string code, ITranslator module);

        /// <summary>Получить все зарегистрированные плагины</summary>
        /// <returns>Код языка и соответствующий ему плагин</returns>
        IReadOnlyDictionary<string, ITranslator> GetTranslators();
    }
}