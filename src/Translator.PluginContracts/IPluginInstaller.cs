namespace Translator.PluginContracts
{
    /// <summary>Устоновщик плагинов из сборки</summary>
    public interface IPluginInstaller
    {
        /// <summary>Загрузить плагины в хост</summary>
        /// <param name="host">Хост</param>
        void Install(ITranslatorPluginHost host);
    }
}