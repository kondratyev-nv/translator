﻿namespace Translator.PluginContracts
{
    /// <summary>Интерфейс плагина перевода</summary>
    public interface ITranslator
    {
        /// <summary>Перевести выражение</summary>
        /// <param name="input">Выражение</param>
        /// <returns>Перевод на языке плагина</returns>
        string Translate(string input);
    }
}