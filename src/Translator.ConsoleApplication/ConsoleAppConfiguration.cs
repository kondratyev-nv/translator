﻿using System.ComponentModel;
using System.Configuration;
using Translator.Core;

namespace Translator.ConsoleApplication
{
    public static class ConsoleAppConfiguration
    {
        /// <summary>Получить конфигурацию</summary>
        public static IConfiguration FromConfigurationManager()
        {
            return new ConfigurationManagerConfiguration();
        }

        private class ConfigurationManagerConfiguration : IConfiguration
        {
            /// <summary>Получить значение по ключу</summary>
            public string Get(string key)
            {
                return ConfigurationManager.AppSettings.Get(key);
            }

            /// <summary>Получить типизированное значение по ключу</summary>
            public T Get<T>(string key)
            {
                var tc = TypeDescriptor.GetConverter(typeof(T));
                return (T) tc.ConvertFrom(Get(key));
            }
        }
    }
}