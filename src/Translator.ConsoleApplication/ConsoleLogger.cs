using System;
using Translator.Core;

namespace Translator.ConsoleApplication
{
    public class ConsoleLogger<T> : ILogger<T>
    {
        /// <summary>Ошибка</summary>
        public void Error(string message)
        {
            Console.WriteLine("[ERROR]: " + message);
        }

        /// <summary>Информация</summary>
        public void Info(string message)
        {
            Console.WriteLine("[INFO]: " + message);
        }

        /// <summary>Предупреждение</summary>
        public void Warning(string message)
        {
            Console.WriteLine("[WARNING]: " + message);
        }
    }
}