﻿using System;
using System.Linq;
using Translator.Core;
using Translator.Core.Implementation;

namespace Translator.ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var logger = new ConsoleLogger<Program>();
            try
            {
                var translator = GetTranslationService();
                Console.WriteLine($"Available languages: {string.Join(", ", translator.AvailableLangs())}");

                var currentLang = translator.GetDefaultLang();
                while (true)
                {
                    Console.Write($"{currentLang}>");
                    var input = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(input))
                    {
                        continue;
                    }

                    var commandArgs = GetCommandArgs(input);
                    if (!commandArgs.Any())
                    {
                        continue;
                    }

                    var command = commandArgs[0];
                    if (command == "exit")
                    {
                        break;
                    }

                    switch (command)
                    {
                        case "select":
                            SetCurrentLang(commandArgs, translator, ref currentLang);
                            break;
                        case "list":
                            ListAvailable(translator);
                            break;
                        default:
                            Translate(translator, currentLang, input);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private static string[] GetCommandArgs(string input)
        {
            return
                input.Trim()
                     .ToLower()
                     .Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries)
                     .Select(s => s.Trim())
                     .ToArray();
        }

        private static ITranslationService GetTranslationService()
        {
            var configuration = ConsoleAppConfiguration.FromConfigurationManager();
            var configurationProvider = new ConfigurationProvider(configuration);
            var logger = new ConsoleLogger<TranslatorPluginLoader>();
            var host = new TranslatorPluginHost();
            var loader = new TranslatorPluginLoader(logger);
            var defaultLanguageProvider = new DefaultLanguageProvider(host, configurationProvider);
            return
                new TranslationServiceBuilder(host, loader).WithPluginsFrom(
                                                               configurationProvider.GetConfiguration().PluginsPath)
                                                           .WithDefaultLanguageProvider(defaultLanguageProvider)
                                                           .Build();
        }

        private static void ListAvailable(ITranslationService translator)
        {
            Console.WriteLine($"Available languages: {string.Join(", ", translator.AvailableLangs())}");
        }

        private static void SetCurrentLang(string[] args, ITranslationService translator, ref string currentLang)
        {
            if (args.Length <= 1)
            {
                return;
            }

            var lang = args[1];
            if (!translator.AvailableLangs().Contains(lang))
            {
                Console.WriteLine($"No translator for code \"{lang}\" found.");
            }
            else
            {
                currentLang = lang;
            }
        }

        private static void Translate(ITranslationService translator, string currentLang, string input)
        {
            var result = translator.Translate(currentLang, input);
            Console.WriteLine($"[{result.Lang}] {result.Result}");
        }
    }
}