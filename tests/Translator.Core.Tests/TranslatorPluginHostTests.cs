﻿using NSubstitute;
using Translator.Core.Implementation;
using Translator.PluginContracts;
using Xunit;

namespace Translator.Core.Tests
{
    public class TranslatorPluginHostTests
    {
        [Fact]
        public void CanRegisterTranslator()
        {
            var translator = Substitute.For<ITranslator>();

            var host = new TranslatorPluginHost();
            host.Register("en", translator);

            var translators = host.GetTranslators();
            Assert.NotEmpty(translators);
            Assert.Equal(1, translators.Count);
            Assert.True(translators.ContainsKey("en"));
            Assert.Same(translator, translators["en"]);
        }

        [Fact]
        public void CannotRegisterTranslatorsWithSameCode()
        {
            var translator1 = Substitute.For<ITranslator>();
            var translator2 = Substitute.For<ITranslator>();

            var host = new TranslatorPluginHost();
            host.Register("en", translator1);

            Assert.Throws<TranslatorRegistrationException>(() => { host.Register("en", translator2); });
        }

        [Fact]
        public void CannotRegisterNullAsTranslator()
        {
            var host = new TranslatorPluginHost();
            Assert.Throws<TranslatorRegistrationException>(() => { host.Register("en", null); });
        }

        [Fact]
        public void CannotRegisterWithEmptyCode()
        {
            var translator = Substitute.For<ITranslator>();
            var host = new TranslatorPluginHost();
            Assert.Throws<TranslatorRegistrationException>(() => { host.Register(null, translator); });
            Assert.Throws<TranslatorRegistrationException>(() => { host.Register("", translator); });
        }
    }
}