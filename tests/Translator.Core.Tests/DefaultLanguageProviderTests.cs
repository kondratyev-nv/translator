﻿using System;
using System.Collections.Generic;
using NSubstitute;
using Translator.Core.Implementation;
using Translator.PluginContracts;
using Xunit;

namespace Translator.Core.Tests
{
    public class DefaultLanguageProviderTests
    {
        [Fact]
        public void ThrowsWhenNoDefaultLangAndNoAnyDefaultLang()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();
            var translator = Substitute.For<ITranslator>();
            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"en", translator}});
            configuration.GetConfiguration()
                         .Returns(new TranslatorConfiguration {AnyDefaultLang = false, DefaultLang = null});

            Assert.Throws<InvalidOperationException>(() => { new DefaultLanguageProvider(host, configuration); });
        }

        [Fact]
        public void ThrowsWhenNoDefaultLangAndNoSuitableTranslator()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();
            var translator = Substitute.For<ITranslator>();
            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"en", translator}});
            configuration.GetConfiguration()
                         .Returns(new TranslatorConfiguration {AnyDefaultLang = false, DefaultLang = "es"});

            var defaultLanguageProvider = new DefaultLanguageProvider(host, configuration);
            Assert.Throws<InvalidOperationException>(() => { defaultLanguageProvider.GetDefaultLang(); });
        }

        [Fact]
        public void ThrowsWhenAnyDefaultLangAndNoTranslators()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();
            host.GetTranslators().Returns(new Dictionary<string, ITranslator>());
            configuration.GetConfiguration()
                         .Returns(new TranslatorConfiguration {AnyDefaultLang = true, DefaultLang = null});

            var defaultLanguageProvider = new DefaultLanguageProvider(host, configuration);
            Assert.Throws<InvalidOperationException>(() => { defaultLanguageProvider.GetDefaultLang(); });
        }

        [Fact]
        public void ThrowsWhenConstructorArgumentsNull()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();

            Assert.Throws<ArgumentNullException>(() => { new DefaultLanguageProvider(null, null); });
            Assert.Throws<ArgumentNullException>(() => { new DefaultLanguageProvider(null, configuration); });
            Assert.Throws<ArgumentNullException>(() => { new DefaultLanguageProvider(host, null); });
        }

        [Fact]
        public void ReturnsDefaultLangWhenProvided()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();
            var translator = Substitute.For<ITranslator>();
            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"en", translator}});
            configuration.GetConfiguration()
                         .Returns(new TranslatorConfiguration {AnyDefaultLang = false, DefaultLang = "en"});

            var defaultLanguageProvider = new DefaultLanguageProvider(host, configuration);
            Assert.Equal("en", defaultLanguageProvider.GetDefaultLang());
        }

        [Fact]
        public void ReturnsDefaultLangWhenAnyDefaultLangProvided()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var configuration = Substitute.For<IConfigurationProvider>();
            var translator = Substitute.For<ITranslator>();
            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"en", translator}});
            configuration.GetConfiguration().Returns(new TranslatorConfiguration {AnyDefaultLang = true});

            var defaultLanguageProvider = new DefaultLanguageProvider(host, configuration);
            Assert.Equal("en", defaultLanguageProvider.GetDefaultLang());
        }
    }
}