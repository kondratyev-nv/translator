﻿using System;
using System.Collections.Generic;
using NSubstitute;
using Translator.Core.Implementation;
using Translator.PluginContracts;
using Xunit;

namespace Translator.Core.Tests
{
    public class TranslationServiceTests
    {
        [Fact]
        public void ThrowsWhenConstructorArgumentsNull()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var defaultLanguageProvider = Substitute.For<IDefaultLanguageProvider>();

            Assert.Throws<ArgumentNullException>(() => { new TranslationService(null, null); });
            Assert.Throws<ArgumentNullException>(() => { new TranslationService(null, defaultLanguageProvider); });
            Assert.Throws<ArgumentNullException>(() => { new TranslationService(host, null); });
        }

        [Fact]
        public void ThrowsWhenNoDefaultLang()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var defaultLanguageProvider = Substitute.For<IDefaultLanguageProvider>();

            defaultLanguageProvider.GetDefaultLang().Returns((string) null);
            Assert.Throws<ArgumentException>(() => { new TranslationService(host, defaultLanguageProvider); });
        }

        [Fact]
        public void ThrowsWhenNoDefaultLangInTranslators()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var defaultLanguageProvider = Substitute.For<IDefaultLanguageProvider>();
            var translator = Substitute.For<ITranslator>();

            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"es", translator}});
            defaultLanguageProvider.GetDefaultLang().Returns("en");
            Assert.Throws<ArgumentException>(() => { new TranslationService(host, defaultLanguageProvider); });
        }

        [Fact]
        public void ReturnTranslationOnSpecifiedLanguage()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var defaultLanguageProvider = Substitute.For<IDefaultLanguageProvider>();
            var translator = Substitute.For<ITranslator>();

            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"es", translator}, {"en", translator}});
            defaultLanguageProvider.GetDefaultLang().Returns("en");
            translator.Translate("привет").Returns("hello");

            var translationService = new TranslationService(host, defaultLanguageProvider);
            var translationResult = translationService.Translate("es", "привет");

            Assert.NotNull(translationResult);
            Assert.Equal("es", translationResult.Lang);
            Assert.Equal("hello", translationResult.Result);
        }

        [Fact]
        public void ReturnTranslationOnDefaultLanguage()
        {
            var host = Substitute.For<ITranslatorPluginHost>();
            var defaultLanguageProvider = Substitute.For<IDefaultLanguageProvider>();
            var translator = Substitute.For<ITranslator>();

            host.GetTranslators().Returns(new Dictionary<string, ITranslator> {{"en", translator}});
            defaultLanguageProvider.GetDefaultLang().Returns("en");
            translator.Translate("привет").Returns("hello");

            var translationService = new TranslationService(host, defaultLanguageProvider);
            var translationResult = translationService.Translate("es", "привет");

            Assert.NotNull(translationResult);
            Assert.Equal("en", translationResult.Lang);
            Assert.Equal("hello", translationResult.Result);
        }
    }
}